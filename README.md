## Activate Sensors + MySQL
```
$ sudo modprobe w1-gpio
$ sudo modprobe w1-therm
$ ls /sys/bus/w1/devices
$ sudo nano /boot/config.txt
$ sudo reboot
$ sudo apt install python-pip
$ sudo pip install MySQL-python
$ pip install mysql-connector-python
$ sudo nano /etc/crontab

edit:
*/5  *    * * *   pi      python /home/pi/Heizung/solarueberwachung.py 
```

## Install Apache2 + PHP7.4
```
$ sudo apt -y install lsb-release apt-transport-https ca-certificates wget
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo apt update
$ sudo apt install apache2 php7.4 php7.4-{common,mysql,xml,xmlrpc,curl,gd,cli,dev,imap,mbstring,opcache,soap,zip,intl,bcmath} -y
```

## Activate SSL Encryption (HTTPS)
```
$ sudo apt install apache2
$ a2enmod ssl
$ a2ensite default-ssl
$ sudo service apache2 reload
$ mkdir /etc/apache2/ssl
$ openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt
$ chmod 600 /etc/apache2/ssl/*
$ sudo nano /etc/apache2/sites-enabled/default-ssl.conf

edit:
ServerName <IP>:443
SSLCertificateFile /etc/apache2/ssl/apache.crt
SSLCertificateKeyFile /etc/apache2/ssl/apache.key

$ sudo service apache2 reload

//Test
$ openssl s_client -connect <IP>:443
```

## Install CA-Certificate
```
$ sudo apt install libnss3-tools build-essential curl file git
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"```

// Test the installation of Homebrew
$ test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
$ test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
$ test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
$ echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
$ brew install gcc

// Install mkcert
$ brew install mkcert