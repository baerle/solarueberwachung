<?php

$db = mysqli_connect("localhost", "solarueberwachung", "solarueberwachung", "solarueberwachung"); # "db = Variable; "wo", "Nutzer", "Passwort", "DB-Name"

$query = 'SELECT * FROM solarueberwachung WHERE datetime > "' . date("Y-m-d") . '";';

$result = mysqli_query($db, $query);

$data = [];
$date = [];

/**
 * @param $i
 * @param $row
 * @return StdClass
 */
function packInStdClass($i, $row)
{
    $obj = new StdClass();
    $obj->x = $i;
    $obj->y = (int)$row;

    return $obj;
}

$i = 0;
while ($row = mysqli_fetch_row($result)) {
    $date[] = $row[1];
    $data['SolarVorlauf'][] = packInStdClass($i, $row[2]);
    $data['SolarRuecklauf'][] = packInStdClass($i, $row[3]);
    $data['SpeicherUnten'][] = $row[4];
    $data['SpeicherOben'][] = $row[5];
    $data['Pumpe'][] = $row[6] ? 18 : 16; //? bewirkt if-else; 15 = wenn true, 10 = wenn false
    $i++;
}

//echo json_encode($data);

/*
$month = date("m"); # weißt der Variablen month den aktuellen Monat zu
$year = date("Y");  # weißt der Variablen year das aktuelle Jahr zu

if ($month == '01') {
    $month = 13;
    $year = $year - 1;
}

$query = "SELECT SUM(Pumpe) FROM solarueberwachung WHERE datetime > '" . $year . "-" . ($month - 1) . "-01' AND datetime < '" . $year . "-" . ($month - 1) . "-31';";

$result = mysqli_query($db, $query);
$row = mysqli_fetch_row($result);  # das sind schon die aufsummierten Werte eines Monats

var_dump($row);
//(mail("dietmar.baer@web.de", "Solarpumpenlaufzeit im Monat ".date("M"), $row);
*/

?>
<html lang="de">
<head>
    <link type="style/css" href="./Chart.js/Chart.css"/>
    <script src="./Chart.js/Chart.bundle.js"></script>
    <!--    <script src="./node_modules/jquery/dist/jquery.min.js"></script>-->
    <title>Tagesauswertung</title>
    <script>
        const PHPData = <?= json_encode($data) ?>;
        const date = <?= json_encode($date) ?>;

        let data = [];

        document.addEventListener("DOMContentLoaded", function () {

            /*for (let item in PHPData) {
                console.log(item);
                let dataline = [];
                document.getElementById("test").textContent = item;
                let i = 0;
                for (let [itemKey, itemValue] in Object.entries(item)) {
                    console.log(itemKey, itemValue);
                    dataline.push({'x': i, 'y': itemValue});
                    i++;
                }
                data.push(dataline);
            }
            console.log(data);*/


            new Chart(document.getElementById('myChart'), {
                type: 'line',
                data: {
                    labels: <?= json_encode($date) ?>,
                    datasets: [{
                        label: 'Solar Vorlauf',
                        data: <?= json_encode($data['SolarVorlauf']) ?>,
                        borderColor: 'rgba(30, 144, 255, 1)',
                        fill: false,
                        borderWidth: 1,
                        lineTension: 0.2
                    }, {
                        label: 'Solar Rücklauf',
                        data: <?= json_encode($data['SolarRuecklauf']) ?>,
                        borderColor: 'rgba(255, 0, 0, 1)',
                        fill: false,
                        borderWidth: 1,
                        lineTension: 0.2
                    }, {
                        label: 'Speicher Oben',
                        data: <?= json_encode($data['SpeicherOben']) ?>,
                        borderColor: 'rgba(34, 139, 34, 1)',
                        fill: false,
                        borderWidth: 1,
                        lineTension: 0.2
                    }, {
                        label: 'Speicher unten = Warmwassertemp.',
                        data: <?= json_encode($data['SpeicherUnten']) ?>,
                        borderColor: 'rgba(0, 0, 205, 1)',
                        fill: false,
                        borderWidth: 1,
                        lineTension: 0.2
                    }, {
                        label: 'Pumpe',
                        data: <?= json_encode($data['Pumpe']) ?>,
                        borderColor: 'rgba(153, 102, 255, 1)',
                        fill: false,
                        borderWidth: 1,
                        lineTension: 0.2
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{}]
                    }
                }
            });
        });
    </script>
</head>

<body>
<canvas id="myChart" width="1350" height="650"></canvas>
<div id="test"></div>
</body>
</html>
