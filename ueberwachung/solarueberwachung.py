#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import fnmatch
import time
import MySQLdb
import logging
from datetime import datetime
import RPi.GPIO as GPIO

logging.basicConfig(filename='/home/pi/solarueberwachung/DS18B20_error.log',
  level=logging.DEBUG,
  format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger=logging.getLogger(__name__)

def insertDB(datetime, temperature, pumpe):

  query = "INSERT INTO solarueberwachung (datetime, SolarVorlauf, SolarRuecklauf, SpeicherUnten, SpeicherOben, Pumpe) VALUES (%s, %s, %s, %s, %s, %s);"

  args = (datetime, temperature[2], temperature[1], temperature[0], temperature[3], pumpe)

  try:
    con = MySQLdb.connect(host='localhost',database='solarueberwachung',user='solarueberwachung',password='solarueberwachung')
    cursor = con.cursor()

    cursor.execute(query, args)

    con.commit()

  finally:
    cursor.close()
    con.close()


# Get readings from sensors and store them in MySQL
now = datetime.now()  # datetime = Klasse, in der die Funktion "now()" aufgerufen wird. Diese gibt ein Objekt an die Var "now" weiter.
zeitstempel = now.strftime('%Y-%m-%d %H:%M:%S') # gibt das aktuelle Datum + Uhrzeit aus
#print datetime

datetime = []
sensornummer = []
temperature = []
IDs = []

for sensorname in os.listdir("/sys/bus/w1/devices"):
  if fnmatch.fnmatch(sensorname, '28-*'):
    with open("/sys/bus/w1/devices/" + sensorname + "/w1_slave") as sensor:
      lines = sensor.readlines()
      if lines[0].find("YES"):
        pok = lines[1].find('=')
        temperature.append(lines[1][pok+1:pok+3])
        IDs.append(sensorname)
        datetime.append(zeitstempel)

        if sensorname == '28-031702ba4dff':
          sensornummer.append("Speicher oben")
        if sensorname == '28-041702cdd7ff':
          sensornummer.append("Speicher unten")
        if sensorname == '28-0000053b9158':
          sensornummer.append("Solar Vorlauf")
        if sensorname == '28-0000053bba61':
          sensornummer.append("Solar Ruecklauf")
        
      else:
        logger.error("Error reading sensor with ID: %s" % (sensorname))

GPIO.setmode(GPIO.BCM)
GPIO.setup(27, GPIO.IN)
led = GPIO.input(27)
if led == 0:  #der LDR liefert den reziproken Wert, daher wird dieser in der nächsten Zeile umgedreht
  led = 1
  print "Pumpe ist an"
else: 
  led = 0
  print "Pumpe ist aus"

if (len(temperature)>0):
  insertDB(zeitstempel, temperature, led)
  print(IDs, sensornummer, zeitstempel, temperature)
