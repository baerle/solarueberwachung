create user solarueberwachung identified by 'solarueberwachung';

create table if not exists solarueberwachung (
    id int auto_increment primary key,
    datetime datetime not null unique,
    SolarVorlauf int(3),
    SolarRuecklauf int(3),
    SpeicherUnten int(3),
    SpeicherOben int(3),
    Pumpe int(1)
) engine=innodb;

create table if not exists laufzeit (
    id int auto_increment primary key,
    Datum datetime not null unique,
    Laufzeit int not null
) engine=innodb;

grant all on solarueberwachung to solarueberwachung;